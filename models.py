# ORM
from peewee import *

# Env
from env import DATABASE

db = SqliteDatabase(DATABASE)

# Models
class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    public_id = TextField(unique = True)
    username = CharField(unique = True, max_length=50)
    password = CharField(max_length=255)

    class Meta:
        table_name = 'Usuario'


class Note(BaseModel):
    user = ForeignKeyField(User)
    description = TextField()

    class Meta:
        table_name = 'Nota'