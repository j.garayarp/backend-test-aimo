from bottle import request, response, abort

# JSON Web Token
import jwt

# Models
from models import *

# Env
from env import SECRET_KEY


secret_key = SECRET_KEY

# Decorators
def token_required(f):
    # @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
            return abort(401, 'No se está enviando token')
        try:
            data = jwt.decode(token, secret_key, algorithms=["HS256"])
            current_user = User.select().where(User.public_id == data['public_id']).first()
        except:
            return abort(401, 'El token es inválido!')
        return f(current_user, *args, **kwargs)
    return decorated

def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, x-access-token'

        if request.method != 'OPTIONS':
            return fn(*args, **kwargs)

        # return fn(*args, **kwargs)

    return _enable_cors