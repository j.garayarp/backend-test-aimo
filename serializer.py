from marshmallow import Schema, fields, validate, ValidationError


class UserSchema(Schema):
    username = fields.Str(
        required=True,
        error_messages={'required': 'Nombre de usuario es requerido.'}
    )
    password = fields.Str(
        required=True, 
        validate=validate.Length(min=8),
        error_messages={
            'required': 'Password es requerido.',
            'min_length': 'El password debe ser mínimo de 8 caracteres'
        }
    )


class NoteSchema(Schema):
    description = fields.Str(
        required=True,
        error_messages={'required': 'Descripción de la nota es requerido.'}
    )
