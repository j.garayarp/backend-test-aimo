# Run with "python server.py"

from bottle import request, route, run, response, abort

# Serializer
from serializer import *

# Models
from models import *

# Decorators
from decorators import *

# JSON Web Token
import jwt

# Tools
import datetime
import json

# Env
from env import SECRET_KEY

# Declare secret key
secret_key = SECRET_KEY


# API
@route('/login', method=['POST', 'OPTIONS'])
@enable_cors
def login():
    if request.method == 'OPTIONS':
        response.headers['Allow'] = 'GET, POST, OPTIONS'
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        return response
    else:
        data = request.json or {}
        errors = UserSchema().validate(data)
        if errors:
            response.status = 400
            response.content_type = 'application/json'
            response.body = json.dumps(errors)
            return response
        user = User.select().where(User.username == data['username'], User.password == data['password']).first()
        if not user:
            response.status = 400
            response.body = 'No existe el usuario en la base de datos.'
            return response
        token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, secret_key, algorithm="HS256")
        response.status = 200
        response.content_type = 'application/json'
        response.body = json.dumps({'token' : token.decode('UTF-8')})
        return response

@route('/notes', method=['OPTIONS'])
@enable_cors
def notes():
    response.headers['Allow'] = 'GET, POST, OPTIONS'
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, x-access-token'
    return response

@route('/notes', method=['GET', 'POST'])
@token_required
@enable_cors
def notes(current_user):
    if request.method == 'GET': # List notes
        notes = Note.select().where(Note.user == current_user.id)
        output = NoteSchema(many=True).dump(notes)
        response.status = 200
        response.content_type = 'application/json'
        response.body = json.dumps(output.data)
        return response
    else: # POST: Create note
        data = request.json or {}
        errors = NoteSchema().validate(data)
        if errors:
            response.status = 400
            response.content_type = 'application/json'
            response.body = json.dumps(errors)
            return response
        new_note = Note(user=current_user.id, description=data['description'])
        new_note.save()
        note_json = NoteSchema().dump(new_note)
        response.status = 201
        response.content_type = 'application/json'
        response.body = json.dumps(note_json.data)
        return response

run(host='localhost', port=8000, debug=True)